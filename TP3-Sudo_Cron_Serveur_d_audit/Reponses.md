# 1. Utilisation de `sudo`

*Faites en sorte que l'utilisateur `etudiant` puisse attribuer une adresse IP 
à sa machine sans avoir à passer root et sans avoir à taper de mot de passe.*

```
su -
visudo
```

Ajouter la ligne :

```
etudiant	ALL=	NOPASSWD:/usr/sbin/ifconfig
```

*Lui donner aussi le droit d'exécuter toute commande dans /usr/sbin en tant
que root avec le mot de passe.*

Changer la ligne en :

```
etudiant	ALL=	/usr/bin/*,NOPASSWD:/urs/sbin/ifconfig
```

*Prenez-vous des risques ?*

Oui, on vient de donner **tout** les droits à l'utilisateur `etudiant`.

# 2. Utilisation de `crontab`

Créer un script `backup_home.sh` dans `/root` :

```sh
#!/bin/bash

tar czf /root/home.bak.tar.gz /home
```

Ce script compresse le dossier `/home` dans le fichier `/root/home.bak.tar.gz`.
**Il est important que le fichier de sauvegarde soit dans un autre dossier que `/home`, si non le sauvegarde suivante va sauvegarder le fichier de sauvegarde.**

Ajouter cette ligne à `/etc/crontab` :

```
*/2 * * * * root /root/backup_home.sh
```
