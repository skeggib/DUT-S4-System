tar             Gestionnaire d'archive
compress        Compression de fichiers ?
gzip            Compression de fichiers
bzip2           Compression de fichiers avec l'algorithme Burrows-Wheeler
find            Recherche de fichiers
su              Changement d'utilisateur
pwd             Afficher le nom du repertoire actuel
grep            Afficher le lignes correspondantes à un modèle donné
cmp             Comparer deux fichiers octet par octet
du              Evaluer la taille des fichiers
df              Indiquer l'espace occupé par les systèmes de fichiers
diff            Comparer deux fichiers ligne par ligne
cat             Concaténer des fichiers et les afficher via la sortie standart
info            Pages de manuel (compément de man)
useradd         Ajouter un compte utilisateur ou lui attribuer un groupe
usermod         Modifier un compte utilisateur
userdel         Supprimer un compte utilisateur
groups          Afficher les groupes auquels appartient un utilisateur
groupadd        Ajouter un groupe
groupmod        Modifier un groupe
groupdel        Supprimer un groupe
chmod           Modifier les droits d'un fichier
chown           Modifier le propriétaire d'un fichier
chgrp           Modifier le groupe propriaitaire d'un fichier
chroot          Executer une commande ou un shell dans un répertoire racine
                fermé
newgrp          Se connecter è un nouveau groupe
passwd          Modifier le mot de passe d'un utilisateur
